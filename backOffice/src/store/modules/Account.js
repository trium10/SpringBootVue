import AccountJson from "@/assets/sampleData/account.json";

const state = {
  Accounts: AccountJson
};
const getters = {};
const mutations = {};
const actions = {};

export default {
  state,
  getters,
  mutations,
  actions
};

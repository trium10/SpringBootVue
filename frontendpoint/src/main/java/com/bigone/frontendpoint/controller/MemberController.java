package com.bigone.frontendpoint.controller;

import java.util.List;

import com.bigone.frontendpoint.repository.MemberRepository;
import com.bigone.frontendpoint.model.Member;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MemberController {

    @Autowired
    MemberRepository memberRepository;

    @GetMapping(path = "/front/Members")
    public List<Member> getMembers() {
        return memberRepository.findAll();
    }

    @GetMapping(path = "/front/Member/{id}", produces = { "application/json" })
    public Member getMember(@PathVariable("id") int id) {
        Member member = memberRepository.findByMemberID(id);

        return member;
    }

    @PostMapping(path = "/front/Member", consumes = { "application/json" })
    public Member AddMember(Member member) {
        memberRepository.save(member);

        return member;
    }

    @DeleteMapping(path = "/front/Member/{id}", produces = { "application/json" })
    public Member DeleteMember(@PathVariable("id") int id) {
        Member member = memberRepository.findByMemberID(id);

        memberRepository.delete(member);

        return member;
    }

    @PutMapping(path = "/front/Member", consumes = { "application/json" })
    public Member InsertOrUpdateMember(Member member) {
        memberRepository.save(member);

        return member;
    }
}
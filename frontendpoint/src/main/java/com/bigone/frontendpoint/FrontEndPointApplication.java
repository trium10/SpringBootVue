package com.bigone.frontendpoint;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FrontEndPointApplication {

	public static void main(String[] args) {
		SpringApplication.run(FrontEndPointApplication.class, args);
	}
}

package com.bigone.frontendpoint.repository;

import com.bigone.frontendpoint.model.Member;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MemberRepository extends JpaRepository<Member, Integer> {
    Member findByMemberID(int memberID);
}